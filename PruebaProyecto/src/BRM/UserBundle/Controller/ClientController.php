<?php

namespace BRM\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use BRM\UserBundle\Entity\Client;
use BRM\UserBundle\Form\ClientType;

class ClientController extends Controller
{
    public function indexAction()
    {
        $br= $this->getDoctrine()->getManager();

        $clients= $br->getRepository('BRMUserBundle:Client')->findAll();

        return $this->render('BRMUserBundle:Client:index.html.twig', array('clients'=> $clients));
    }

    public function addAction()
    {
        $client=new Client();
        $form=$this->createCreateForm($client);
        
        return $this->render('BRMUserBundle:Client:add.html.twig', array('form' => $form->createView()));
    }

    private function createCreateForm(Client $entity)
    {
        $form=$this->createForm(new ClientType(), $entity, array(
            'action'=>$this->generateUrl('brm_client_create'),
            'method'=>'POST'));

        return $form;
    }

    public function createAction(Request $request)
    {
        $client= new Client();
        $form= $this->createCreateForm($client);
        $form->handleRequest($request);

        if($form->isValid())
        {
            $br= $this->getDoctrine()->getManager();
            $br->persist($client);
            $br->flush();

            //return $this->redirectToRoute('brm_user_index');
            return $this->redirect($this->generateUrl('brm_client_index'));

        }

        return $this->render('BRMUserBundle:Client:add.html.twig', array('form' => $form->createView())); 
    }

    public function viewAction($id)
    {
        $repository=$this->getDoctrine()->getRepository('BRMUserBundle:Client');

        $client=$repository->find($id);

        return new Response('Usuario: '.$client->getIdDocument().' con email : '.$client->getUser());
    }
}
