<?php

namespace BRM\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use BRM\UserBundle\Entity\User;
use BRM\UserBundle\Form\UserType;

class UserController extends Controller
{
    public function indexAction()
    {
        $br= $this->getDoctrine()->getManager();

        $users= $br->getRepository('BRMUserBundle:User')->findAll();
        /*
        $res='Lista de Usuarios: <br/>';

        foreach($users as $user)
        {
            $res.='Usuario: '.$user->getUsername().' - Email: '.$user->getEmail().'<br/>';
        }

        return new Response($res);*/

        return $this->render('BRMUserBundle:User:index.html.twig', array('users'=> $users));
    }

    public function addAction()
    {
        $user=new User();
        $form=$this->createCreateForm($user);
        
        return $this->render('BRMUserBundle:User:add.html.twig', array('form' => $form->createView()));
    }

    private function createCreateForm(User $entity)
    {
        $form=$this->createForm(new UserType(), $entity, array('action'=>$this->generateUrl('brm_user_create'),'method'=>'POST'));

        return $form;
    }

    public function createAction(Request $request)
    {
        $user= new User();
        $form= $this->createCreateForm($user);
        $form->handleRequest($request);

        if($form->isValid())
        {
            /*$password=$form->get('password')->getData();

            $encoder=$this->container->get('security.password_encoder');
            $encoded=$encoder->encodePassword($user,$password);

            $user->setPassword($encoded);*/

            $br= $this->getDoctrine()->getManager();
            $br->persist($user);
            $br->flush();

            //return $this->redirectToRoute('brm_user_index');
            return $this->redirect($this->generateUrl('brm_user_index'));

        }

        return $this->render('BRMUserBundle:User:add.html.twig', array('form' => $form->createView())); 
    }

    public function editAction($id)
    {
        $br=$this->getDoctrine()->getManager();
        $user=$br->getRepository('BRMUserBundle:User')->find($id);

        if(!$user)
        {
            throw $this->createNotFoundException('User not found');
        }

        $form= $this->createEditForm($user);

        return $this->render('BRMUserBundle:User:edit.html.twig', array('user' => $user, 'form' => $form->createView()));
    }

    private function createEditForm(User $entity)
    {
        $form= $this->createForm(new UserType(), $entity, array('action' => $this->generateUrl('brm_user_update', array('id' => $entity->getId())), 'method' => 'PUT'));

        return $form;
    }

    public function updateAction($id, Request $request)
    {
        $br= $this->getDoctrine()->getManager();

        $user=$br->getRepository('BRMUserBundle:User')->find($id);

        if(!$user)
        {
            throw $this->createNotFoundException('User not found');
        }

        $form= $this->createEditForm($user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $password=$form->get('password')->getData();

            if(empty($password))
            {
                $recoverPass= $this->recoverPass($id);
                $user->setPassword($recoverPass[0]['password']);
            }
            if($form->get('role')->getData()=='ROLE_ADMIN')
            {
                $user->setIsActive(1);
            }
            $br->flush();

            //$this->addFlash('mensaje','The user has been modified');
            return $this->redirect($this->generateUrl('brm_user_index',array('id'=>$user->getId())));
        }
        return $this->render('BRMUserBundle:User:edit.html.twig', array('user'=> $user, 'form' => $form->createView())); 

    }

    private function recoverPass($id)
    {   
        $br=$this->getDoctrine()->getManager();
        $query=$br->createQuery('SELECT u.password FROM BRMUserBundle:User u WHERE u.id=:id')->setParemeter('id',$id);

        $currentPass=$query->getResult();

        return $currentPass;
    }
    public function viewAction($id)
    {
        $repository=$this->getDoctrine()->getRepository('BRMUserBundle:User');

        $user=$repository->find($id);

        return new Response('Usuario: '.$user->getUsername().' con email : '.$user->getEmail());
    }
}
