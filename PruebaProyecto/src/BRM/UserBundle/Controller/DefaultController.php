<?php

namespace BRM\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
   /* public function indexAction($name)
    {
        return $this->render('BRMUserBundle:Default:index.html.twig', array('name' => $name));
    }*/

    public function homeAction()
    {
        return $this->render('BRMUserBundle:Default:index.html.twig');
    }

    public function aboutAction()
    {
        return $this->render('BRMUserBundle:Default:about.html.twig');
    }
}
